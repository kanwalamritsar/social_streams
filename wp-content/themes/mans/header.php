<?php
define(CONSTANT_BLOG,4);
define(CONSTANT_PRODUCT,1);
define(CONSTANT_FEATURED,3);
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage mansshoppingcart
 * @since Twenty Thirteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link href="<?php echo get_template_directory_uri(); ?>/css/mans-styles.css" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php //wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<div id="page" class="hfeed site">
    <header class="header" id="header">
		<div class="header-top">
			<div class="container">
				<div class="row">
					<div class="header-top__container">
						<nav class="header-top__nav">
                        <?php wp_nav_menu(array('theme_location' => 'in_header','fallback_cb' => 'customHeaderMenu')); ?>
							
						</nav>
					 </div>
				</div>
			</div>
		</div><!-- end .header-top -->
		<div class="header-middle">
			<div class="container">
				<div class="row">
					<div class="header-middle__logo"><a href="<?php echo get_site_url(); ?>" style="text-decoration:none; color:#ffffff;"  ><h1 class="logocustomstyle">Stack<span style="color:#ADD8E6;">Savings</span></h1><small class="logotagline">Affordable custom programming</small></a></div>
					<div class="header-middle__cart" id="header-cart">
						<div class="header-middle__cart__container">
							<!--<div class="header-middle__cart__status icon-cart icon-arrow-small">
								<strong>Your cart:</strong>
								<span id="hm-cart-items">3 items on $1900</span>
							</div>-->
							<div class="cart__sub">
								<ul class="cart__sub__list">
									<li class="cart__sub__item">
										<a class="cart__sub__product" href="#">
											<span class="cart__sub__photo"><img src="http://placehold.it/76x61" alt=""/></span>
											<span class="cart__sub__info">
												<span class="cart__sub__title">There are many variations</span>
												<span class="cart__sub__price">$1400</span>
											</span>
										</a>
										<a class="cart__sub__close icon-close-dark" href="#">close</a>
									</li>
									<li class="cart__sub__item">
										<a class="cart__sub__product" href="#">
											<span class="cart__sub__photo"><img src="http://placehold.it/76x61" alt=""/></span>
											<span class="cart__sub__info">
												<span class="cart__sub__title">There are many variations</span>
												<span class="cart__sub__price">$1400</span>
											</span>
										</a>
										<a class="cart__sub__close icon-close-dark" href="#">close</a>
									</li>
									<li class="cart__sub__item">
										<a class="cart__sub__product" href="#">
											<span class="cart__sub__photo"><img src="http://placehold.it/76x61" alt=""/></span>
											<span class="cart__sub__info">
												<span class="cart__sub__title">There are many variations</span>
												<span class="cart__sub__price">$1400</span>
											</span>
										</a>
										<a class="cart__sub__close icon-close-dark" href="#">close</a>
									</li>
								</ul>
								<div class="cart__sub__setting">
									<ul class="horizontal">
										<li><a class="btn" href="#">My cart</a></li>
										<li><a class="btn gray" id="cart-sub-clear" href="#">Clear</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="header-middle__nav" id="header-middle-nav">
						<a class="icon-menu" href="#">
							<span class="menu-line">
								<span class="line"></span>
								<span class="line"></span>
								<span class="line"></span>
							</span>
							Menu
						</a>
					</div>
				</div>
			</div>
		</div><!-- end .header-middle -->
		<nav class="hm-nav__sub small" id="header-middle-nav-sub">
			<div class="container">
				<div class="row">
					<div class="col-xs-6 col-sm-3">
						<ul>
						<?php  $allcategories = get_categories(); 
						foreach($allcategories as $singleCat) { 
						   if($singleCat->parent==0){
 						?> 
                         
                        <li><?php echo '<p><a href="' . get_category_link( $singleCat->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $singleCat->name ) . '" ' . '>' . $singleCat->name.'</a></p> ';
?></li>
                        <?php //$catPost = get_posts(array('category'=>$singleCat->cat_ID));?>
                        <?php 
							$catData = get_category($singleCat->cat_ID);
							$args = array(	'child_of'=>$catData->term_id); 
							$allSubCategories = get_categories($args);
							if(count($allSubCategories)>0){ ?>
	                            <ul class="hm-nav__sub__list">
						  			<?php foreach ($allSubCategories as $subCat) { ?>
                                
                        	<li><a href="<?php echo get_category_link( $subCat->cat_ID ); ?>"><?php echo $subCat->name;?> </a><span class="count-number">2</span></li>
							<?php } ?>
						</ul>
                        <?php };
						} 
						}?>
                        </ul>
						<!--<p>Laptops &amp; Notebook</p>
						<ul class="hm-nav__sub__list">
							<li><a href="#">Macs</a><span class="count-number">2</span></li>
							<li><a href="#">Windows</a><span class="count-number">32</span></li>
							<li><a href="#">Components</a><span class="count-number">22</span></li>
							<li><a href="#">Mice and Trackballs</a><span class="count-number">12 089</span></li>
							<li><a href="#">Monitors</a><span class="count-number">200</span></li>
							<li><a href="#">Printers</a><span class="count-number">13</span></li>
							<li><a href="#">Scanners</a><span class="count-number">4</span></li>
							<li><a href="#">Web Cameras</a><span class="count-number">98</span></li>
						</ul>
						<p>Components</p>
						<ul class="hm-nav__sub__list">
							<li><a href="#">Mice and Trackballs</a><span class="count-number">2</span></li>
							<li><a href="#">Monitors</a><span class="count-number">34</span></li>
							<li><a href="#">Printers</a><span class="count-number">2</span></li>
							<li><a href="#">Scanners</a><span class="count-number">956</span></li>
							<li><a href="#">Web Cameras</a><span class="count-number">2</span></li>
						</ul>
					</div>
					<div class="col-xs-6 col-sm-3">
						<p>Components</p>
						<ul class="hm-nav__sub__list">
							<li><a href="#">Mice and Trackballs</a><span class="count-number">2</span></li>
							<li><a href="#">Monitors</a><span class="count-number">34</span></li>
							<li><a href="#">Printers</a><span class="count-number">2</span></li>
							<li><a href="#">Scanners</a><span class="count-number">956</span></li>
							<li><a href="#">Web Cameras</a><span class="count-number">2</span></li>
						</ul>
						<p>Laptops &amp; Notebook</p>
						<ul class="hm-nav__sub__list">
							<li><a href="#">Macs</a><span class="count-number">2</span></li>
							<li><a href="#">Windows</a><span class="count-number">32</span></li>
							<li><a href="#">Components</a><span class="count-number">22</span></li>
							<li><a href="#">Mice and Trackballs</a><span class="count-number">12 089</span></li>
							<li><a href="#">Monitors</a><span class="count-number">200</span></li>
							<li><a href="#">Printers</a><span class="count-number">13</span></li>
							<li><a href="#">Scanners</a><span class="count-number">4</span></li>
							<li><a href="#">Web Cameras</a><span class="count-number">98</span></li>
						</ul>
						<p>Desktops</p>
						<ul class="hm-nav__sub__list">
							<li><a href="#">PC</a><span class="count-number">2</span></li>
							<li><a href="#">Mac</a><span class="count-number">25</span></li>
						</ul>
					</div>
					<div class="col-xs-6 col-sm-3">
						<p>Desktops</p>
						<ul class="hm-nav__sub__list">
							<li><a href="#">PC</a><span class="count-number">2</span></li>
							<li><a href="#">Mac</a><span class="count-number">25</span></li>
						</ul>
						<p>Laptops &amp; Notebook</p>
						<ul class="hm-nav__sub__list">
							<li><a href="#">Macs</a><span class="count-number">2</span></li>
							<li><a href="#">Windows</a><span class="count-number">32</span></li>
							<li><a href="#">Components</a><span class="count-number">22</span></li>
							<li><a href="#">Mice and Trackballs</a><span class="count-number">12 089</span></li>
							<li><a href="#">Monitors</a><span class="count-number">200</span></li>
							<li><a href="#">Printers</a><span class="count-number">13</span></li>
							<li><a href="#">Scanners</a><span class="count-number">4</span></li>
							<li><a href="#">Web Cameras</a><span class="count-number">98</span></li>
						</ul>
						<p>Components</p>
						<ul class="hm-nav__sub__list">
							<li><a href="#">Mice and Trackballs</a><span class="count-number">2</span></li>
							<li><a href="#">Monitors</a><span class="count-number">34</span></li>
							<li><a href="#">Printers</a><span class="count-number">2</span></li>
							<li><a href="#">Scanners</a><span class="count-number">956</span></li>
							<li><a href="#">Web Cameras</a><span class="count-number">2</span></li>
						</ul>
					</div>
					<div class="col-xs-6 col-sm-3">
						<p>Components</p>
						<ul class="hm-nav__sub__list">
							<li><a href="#">Mice and Trackballs</a><span class="count-number">2</span></li>
							<li><a href="#">Monitors</a><span class="count-number">34</span></li>
							<li><a href="#">Printers</a><span class="count-number">2</span></li>
							<li><a href="#">Scanners</a><span class="count-number">956</span></li>
							<li><a href="#">Web Cameras</a><span class="count-number">2</span></li>
						</ul>
						<p>Laptops &amp; Notebook</p>
						<ul class="hm-nav__sub__list">
							<li><a href="#">Macs</a><span class="count-number">2</span></li>
							<li><a href="#">Windows</a><span class="count-number">32</span></li>
							<li><a href="#">Components</a><span class="count-number">22</span></li>
							<li><a href="#">Mice and Trackballs</a><span class="count-number">12 089</span></li>
							<li><a href="#">Monitors</a><span class="count-number">200</span></li>
							<li><a href="#">Printers</a><span class="count-number">13</span></li>
							<li><a href="#">Scanners</a><span class="count-number">4</span></li>
							<li><a href="#">Web Cameras</a><span class="count-number">98</span></li>
						</ul>
						<p>Desktops</p>
						<ul class="hm-nav__sub__list">
							<li><a href="#">PC</a><span class="count-number">2</span></li>
							<li><a href="#">Mac</a><span class="count-number">25</span></li>
						</ul>-->
                        
                        <!-- start of menu -->
                        		<!--<div id="main" class="site-main">-->
 				<span class="info_menu_media">
 				<p>Info</p>
				<?php  wp_nav_menu(array('fallback_cb' => 'customMobileViewHeaderMenu')); ?>
				
				</span>            
                        <!-- end of menu -->
                        
					</div>
				</div>
			</div>
		</nav><!-- end .hm-nav__sub -->
		<div class="search-sub open">
			<div class="search-sub__container">
				<!--<div class="container">
					<div class="row">
						<h2><span>Search</span> in this shop</h2>
						<div class="search-sub__form clearfix">
							<form action="#" method="post">
								<ul class="search-sub__form__list">
									<li class="search-sub__item big">
										<input type="text" class="input-text search-text" placeholder="Search"/>
									</li>
									<li class="search-sub__item medium">
										<select class="styled select-search">
											<option>Insert search category</option>
											<option>Jewelry &amp; Watches</option>
											<option>Apparel &amp; Accessories</option>
										</select>
									</li>
									<li class="search-sub__item small">
										<button type="submit" class="btn-search icon-search-color">search</button>
									</li>
								</ul>
							</form>
						</div>
					</div>
				</div>-->
			</div>
			<div class="container">
				<div class="row">
					<div class="header-top__search__btn">
						<a id="header-search" class="icon-search-black" href="#">search</a>
					</div>
				</div>
			</div>
		</div><!-- end .search-sub -->
	</header><!-- end .header -->
    
    
    
		<!--<header id="masthead" class="site-header" role="banner">
			<a class="home-link" href="<?php //echo esc_url( home_url( '/' ) ); ?>" title="<?php //echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
				<h1 class="site-title"><?php //bloginfo( 'name' ); ?></h1>
				<h2 class="site-description"><?php //bloginfo( 'description' ); ?></h2>
			</a>

			<!---<div id="navbar" class="navbar">
				<nav id="site-navigation" class="navigation main-navigation" role="navigation">
					<button class="menu-toggle"><?php //_e( 'Menu', 'twentythirteen' ); ?></button>
					<a class="screen-reader-text skip-link" href="#content" title="<?php //esc_attr_e( 'Skip to content', 'twentythirteen' ); ?>"><?php //_e( 'Skip to content', 'twentythirteen' ); ?></a>
					<?php //wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
					<?php //get_search_form(); ?>
				</nav><!-- #site-navigation -->
			<!--</div><!-- #navbar -->
		<!--</header><!-- #masthead -->

