<?php get_header(); ?>
<div class="main" id="main">
		<div class="main-slider">
			<div  class="flexslider "id="main-slider">
				<ul class="slides">
					<li>
						<div class="container">
							<div class="row">
								<div class="col-sm-6">
									<img src="http://placehold.it/627x553" alt=""/>
									<div class="main-slider__info">
										<div class="main-slider__info__middle">
											<h2>Man's products shop for brutal men</h2>
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
											<div class="main-slider__info__btn"><a class="btn medium red" href="#">See catalog</a></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="container">
							<div class="row">
								<div class="col-sm-6">
									<img src="http://placehold.it/627x553" alt=""/>
									<div class="main-slider__info">
										<div class="main-slider__info__middle">
											<h2>Man's products shop for brutal men</h2>
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
											<div class="main-slider__info__btn"><a class="btn medium red" href="#">See catalog</a></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="container">
							<div class="row">
								<div class="col-sm-6">
									<img src="http://placehold.it/627x553" alt=""/>
									<div class="main-slider__info">
										<div class="main-slider__info__middle">
											<h2>Man's products shop for brutal men</h2>
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
											<div class="main-slider__info__btn"><a class="btn medium red" href="#">See catalog</a></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<div class="main-slider__controls" id="main-slider-controls">
				<div class="container">
					<div class="row">
						<div class="col-sm-4">
							<div class="main-slider__box active">
								<img src="http://placehold.it/151x144" alt=""/>
								<div class="main-slider__box__middle">
									<h4>
										<span>THE<br>BEST</span>
										WATHESS
									</h4>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="main-slider__box">
								<img src="http://placehold.it/154x134" alt=""/>
								<div class="main-slider__box__middle">
									<h4>
										<span>jewelry</span>						
										attributes
									</h4>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="main-slider__box">
								<img src="http://placehold.it/111x144" alt=""/>
								<div class="main-slider__box__middle">
									<h4>
										<span>All kinds<br>of</span>
										clothing
									</h4>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="our-advantages" style="padding-top:0px; padding-bottom:0px;">
			<!--<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 wow fadeInUp animated" data-wow-delay="0.2s" data-wow-offset="50" data-wow-duration="1s">
						<div class="our-advantages__box">
							<i class="icon-delivery"></i>
							<h3>International Delivery</h3>
							<p>Over 20 Countries</p>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 wow fadeInUp animated" data-wow-delay="0.4s" data-wow-offset="50" data-wow-duration="1s">
						<div class="our-advantages__box">
							<i class="icon-click"></i>
							<h3>Click &amp; Collect</h3>
							<p>Consectetur Adipiscing Elit</p>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 wow fadeInUp animated" data-wow-delay="0.6s" data-wow-offset="50" data-wow-duration="1s">
						<div class="our-advantages__box">
							<i class="icon-free"></i>
							<h3>FREE Standard delivery</h3>
							<p>Orders $50 and over</p>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 wow fadeInUp animated" data-wow-delay="0.8s" data-wow-offset="50" data-wow-duration="1s">
						<div class="our-advantages__box">
							<i class="icon-secure"></i>
							<h3>Secure Payment</h3>
							<p>Eiusmod Tempor Incididunt</p>
						</div>
					</div>
				</div>
			</div>
		</div>--><!-- end .our-advantages -->
		<div class="best-sellers products-catalog" style="padding-bottom:0px;">
			<div class="best-sellers__mobile-nav visible">
				<a class="menu-line" href="#">
					<span class="line"></span>
					<span class="line"></span>
					<span class="line"></span>
					Best sellers item
				</a>
				<!--<div class="best-sellers__mobile-nav__hide">
					<ul class="item-nav__list">
						<li><a href="#"><span>–</span> Appliances</a></li>
						<li><a href="#"><span>–</span> Baby, Kids, Toys</a></li>
						<li><a href="#"><span>–</span> Computer, Parts</a></li>
						<li><a href="#"><span>–</span> Software</a></li>
						<li><a href="#"><span>–</span> Desktops</a></li>
						<li><a href="#"><span>–</span> Laptops &amp; Notebooks</a></li>
						<li><a href="#"><span>–</span> Components</a></li>
						<li><a href="#"><span>–</span> Tablets</a></li>
						<li><a href="#"><span>–</span> Phones &amp; PDAs</a></li>
						<li><a href="#"><span>–</span> Cameras</a></li>
						<li><a href="#"><span>–</span> Tablets</a></li>
						<li><a href="#"><span>–</span> Phones &amp; PDAs</a></li>
						<li><a href="#"><span>–</span> Components</a></li>
					</ul>
					<div class="item-nav__btn"><a class="btn medium" href="#">All categories</a></div>
				</div>
			</div>-->
			<div class="best-sellers__middle">
				<div id="container">
					<div class="grid-sizer"></div>
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                     	<?php 
						 	if ( has_post_thumbnail() ) {
		 						$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );
								 $url = $thumb['0'];?>
                      <div class="item">
						<div class="item__container">
                 			<a href="<?php the_permalink(); ?>"><img src="<?=$url;?>" alt=""/> </a>
							<span class="item__info">
								<span class="item__info__title">hello <?php echo get_the_title(); ?> </span>
								<span class="item__info__bottom">
									<span class="item__info__price">$1 900</span>
									<a class="item__info__cart icon-cart-small" href="#cart">cart</a>
								</span>
								<a class="item__info__link" href="#product"> hello 
								<?php  get_template_part( 'content', get_post_format()); ?></a>
							</span>
						</div>
					</div>
							<?php }	
							
				endwhile; ?>
           
			<?php else :
			
			get_template_part( 'content', 'none');
			
			endif; ?>
                   
					<!--<div class="item">
						<div class="item__container">
							<img src="http://placehold.it/257x322" alt=""/>
							<span class="item__info">
								<span class="item__info__title">Fashion Black Stainless Steel Luxury</span>
								<span class="item__info__bottom">
									<span class="item__info__price">$1 900</span>
									<a class="item__info__cart icon-cart-small" href="#cart">cart</a>
								</span>
								<a class="item__info__link" href="#product"></a>
							</span>
						</div>
					</div>
					<div class="item item-nav__height">
						<div class="item__container">
							<nav class="item-nav">
								<h3>Popular categories</h3>
								<ul class="item-nav__list">
									<li><a href="#"><span>–</span> Appliances</a></li>
									<li><a href="#"><span>–</span> Baby, Kids, Toys</a></li>
									<li><a href="#"><span>–</span> Computer, Parts</a></li>
									<li><a href="#"><span>–</span> Software</a></li>
									<li><a href="#"><span>–</span> Desktops</a></li>
									<li><a href="#"><span>–</span> Laptops &amp; Notebooks</a></li>
									<li><a href="#"><span>–</span> Components</a></li>
									<li><a href="#"><span>–</span> Tablets</a></li>
									<li><a href="#"><span>–</span> Phones &amp; PDAs</a></li>
									<li><a href="#"><span>–</span> Cameras</a></li>
									<li><a href="#"><span>–</span> Tablets</a></li>
									<li><a href="#"><span>–</span> Phones &amp; PDAs</a></li>
									<li><a href="#"><span>–</span> Components</a></li>
								</ul>
								<div class="item-nav__btn"><a class="btn medium" href="#">All categories</a></div>
							</nav>
						</div>
					</div>
					<div class="item item__height">
						<div class="item__container">	
							<img src="http://placehold.it/257x322" alt=""/>
							<span class="item__info">
								<span class="item__info__title">Fashion Black Stainless Steel Luxury</span>
								<span class="item__info__bottom">
									<span class="item__info__price">$1 900</span>
									<a class="item__info__cart icon-cart-small" href="#cart">cart</a>
								</span>
								<a class="item__info__link" href="#product"></a>
							</span>
						</div>
					</div>
					<div class="item">
						<div class="item__container">
							<img src="http://placehold.it/257x322" alt=""/>
							<span class="item__info">
								<span class="item__info__title">Fashion Black Stainless Steel Luxury</span>
								<span class="item__info__bottom">
									<span class="item__info__price">$1 900</span>
									<a class="item__info__cart icon-cart-small" href="#cart">cart</a>
								</span>
								<a class="item__info__link" href="#product"></a>
							</span>
						</div>
					</div>
					<div class="item">
						<div class="item__container">
							<img src="http://placehold.it/257x172" alt=""/>
							<span class="item__info">
								<span class="item__info__title">Fashion Black Stainless</span>
								<span class="item__info__bottom">
									<span class="item__info__price">$1 900</span>
									<a class="item__info__cart icon-cart-small" href="#cart">cart</a>
								</span>
								<a class="item__info__link" href="#product"></a>
							</span>
						</div>
					</div>
					<div class="item">
						<div class="item__container">
							<img src="http://placehold.it/257x322" alt=""/>
							<span class="item__info">
								<span class="item__info__title">Fashion Black Stainless Steel Luxury</span>
								<span class="item__info__bottom">
									<span class="item__info__price">$1 900</span>
									<a class="item__info__cart icon-cart-small" href="#cart">cart</a>
								</span>
								<a class="item__info__link" href="#product"></a>
							</span>
						</div>
					</div>
					<div class="item">
						<div class="item__container">
							<img src="http://placehold.it/257x322" alt=""/>
							<span class="item__info">
								<span class="item__info__title">Fashion Black Stainless Steel Luxury</span>
								<span class="item__info__bottom">
									<span class="item__info__price">$1 900</span>
									<a class="item__info__cart icon-cart-small" href="#cart">cart</a>
								</span>
								<a class="item__info__link" href="#product"></a>
							</span>
						</div>
					</div>
					<div class="item w2 item__height">
						<div class="item__container">
							<img src="http://placehold.it/533x232" alt=""/>
							<span class="item__info">
								<span class="item__info__title">Fashion Black Stainless Steel Luxury</span>
								<span class="item__info__bottom">
									<span class="item__info__price">$1 900</span>
									<a class="item__info__cart icon-cart-small" href="#cart">cart</a>
								</span>
								<a class="item__info__link" href="#product"></a>
							</span>
						</div>
					</div>	
					<div class="item">
						<div class="item__container">
							<img src="http://placehold.it/257x172" alt=""/>
							<span class="item__info">
								<span class="item__info__title">Fashion Black Stainless</span>
								<span class="item__info__bottom">
									<span class="item__info__price">$1 900</span>
									<a class="item__info__cart icon-cart-small" href="#cart">cart</a>
								</span>
								<a class="item__info__link" href="#product"></a>
							</span>
						</div>
					</div>
					<div class="item">
						<div class="item__container">
							<img src="http://placehold.it/257x434" alt=""/>
							<span class="item__info">
								<span class="item__info__title">Fashion Black Stainless Steel Luxury</span>
								<span class="item__info__bottom">
									<span class="item__info__price">$1 900</span>
									<a class="item__info__cart icon-cart-small" href="#cart">cart</a>
								</span>
								<a class="item__info__link" href="#product"></a>
							</span>
						</div>
					</div>
					<div class="item">
						<div class="item__container">
							<img src="http://placehold.it/257x237" alt=""/>
							<span class="item__info">
								<span class="item__info__title">Fashion Black Stainless Steel Luxury</span>
								<span class="item__info__bottom">
									<span class="item__info__price">$1 900</span>
									<a class="item__info__cart icon-cart-small" href="#cart">cart</a>
								</span>
								<a class="item__info__link" href="#product"></a>
							</span>
						</div>
					</div>
					<div class="item">
						<div class="item__container">
							<img src="http://placehold.it/257x237" alt=""/>
							<span class="item__info">
								<span class="item__info__title">Fashion Black Stainless Steel Luxury</span>
								<span class="item__info__bottom">
									<span class="item__info__price">$1 900</span>
									<a class="item__info__cart icon-cart-small" href="#cart">cart</a>
								</span>
								<a class="item__info__link" href="#product"></a>
							</span>
						</div>
					</div>
					<div class="item item__height">
						<div class="item__container">
							<img src="http://placehold.it/257x174" alt=""/>
							<span class="item__info">
								<span class="item__info__title">Fashion Black Stainles</span>
								<span class="item__info__bottom">
									<span class="item__info__price">$1 900</span>
									<a class="item__info__cart icon-cart-small" href="#cart">cart</a>
								</span>
								<a class="item__info__link" href="#product"></a>
							</span>
						</div>
					</div>
					<div class="item">
						<div class="item__container">
							<img src="http://placehold.it/257x174" alt=""/>
						</div>
						<span class="item__discount">
							<a href="#">
								<span class="item__discount__percentage"><span>-10%</span></span>
							</a>
						</span>
						<span class="item__info">
							<span class="item__info__title">Fashion Black Stainless</span>
							<span class="item__info__bottom">
								<span class="item__info__price">$1 900</span>
								<a class="item__info__cart icon-cart-small" href="#cart">cart</a>
							</span>
							<a class="item__info__link" href="#product"></a>
						</span>
					</div>
				</div>
			</div>
			<div class="best-sellers__bottom">
				<div class="container">
					<div class="row">	
						<p>Investment, implementation non-partisan Gandhi life-saving enable collaborative cities women's rights change movements vaccines cooperation emergency response transform the world.</p>
						<div class="best-sellers__btn">
							<ul class="best-sellers__btn__list horizontal">
								<li class="best-sellers__btn__item"><a class="btn medium red" href="#">About info</a></li>
								<li class="best-sellers__btn__item"><a class="btn medium gray" href="#">See my catalog</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>-->
        <!-- end .best-sellers -->
		<!--<div class="promo-product">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 fadeInUp wow animated" data-wow-delay="0.3s" data-wow-offset="50" data-wow-duration="1s">
						<div class="promo-product__photo"><img src="http://placehold.it/242x402" alt=""/></div>
					</div>
					<div class="col-xs-12 col-sm-6 wow fadeInUp animated" data-wow-delay="0.6s" data-wow-offset="50" data-wow-duration="1s">
						<div class="promo-product__text">
							<h2>
								<span>Fashion</span> Black Stainless<br>
								Steel Luxury Sport Analog<br>
								Quartz <span>Clock Mens</span><br>
								Wrist Watch
							</h2>
							<div class="promo-product__btn"><a class="btn medium" href="#">See my catalog</a></div>
						</div>
					</div>
				</div>
			</div>
		</div>-->
        <!-- end .promo-product -->
		<!--<div class="promo-services">
			<div class="container">
				<div class="row">
					<div class="flexslider carousel" id="promo-services__slider">
						<ul class="slides">
							<li><a href="#"><img src="http://placehold.it/113x26" alt=""/></a></li>
							<li><a href="#"><img src="http://placehold.it/113x26" alt=""/></a></li>
							<li><a href="#"><img src="http://placehold.it/113x26" alt=""/></a></li>
							<li><a href="#"><img src="http://placehold.it/113x26" alt=""/></a></li>
							<li><a href="#"><img src="http://placehold.it/113x26" alt=""/></a></li>
							<li><a href="#"><img src="http://placehold.it/113x26" alt=""/></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>-->
        <!-- end .promo-services -->
		<!--<div class="main-news">
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-sm-6 col-xs-6 wow fadeInUp animated" data-wow-delay="0.2s" data-wow-offset="50" data-wow-duration="1s">
						<div class="main-news__box">
							<div class="main-news__box__photo">
								<a href="#">
									<img src="http://placehold.it/255x356" alt=""/>
								</a>
								<div class="main-news__box__date">23.02.2014</div>
							</div>
							<h3><a href="#">Gas 'N Go Mower</a></h3>
							<p>Investment, implementation non-partisan Gandhi life-saving enable collaborative cities women's rights change ...</p>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 wow fadeInUp animated" data-wow-delay="0.4s" data-wow-offset="50" data-wow-duration="1s">
						<div class="main-news__box">
							<div class="main-news__box__photo">
								<a href="#">
									<img src="http://placehold.it/255x356" alt=""/>
								</a>
								<div class="main-news__box__date">23.02.2014</div>
							</div>
							<h3><a href="#">Fashion Black Stainless Steel Luxury</a></h3>
							<p>Investment, implementation non-partisan Gandhi life-saving enable collaborative cities women's rights change ...</p>
						</div>
					</div>
					<div class="col-md-6  col-sm-12 wow fadeInUp animated" data-wow-delay="0.6s" data-wow-offset="50" data-wow-duration="1s">
						<div class="main-news__box photo-bg">
							<div class="main-news__box__container">
								<h3><a href="#">Fashion Black Stainless Steel Luxury</a></h3>
								<p>Investment, implementation non-partisan Gandhi life-saving enable collaborative cities women's rights change ...</p>
							</div>
							<div class="main-news__box__date">23.02.2014</div>
							<img src="http://placehold.it/540x531" alt=""/>
						</div>
					</div>
				</div>
			</div>
		</div>-->
        <!-- end .main-news -->
		<!--<div class="promo-text">
			<div class="container">
				<div class="row">
					<h2>
						<span>Fashion Black</span>
						<span>Stainless <strong>Steel Luxury</strong></span>
						<span>Sport Analog</span>
					</h2>
				</div>
			</div>-->
		</div>
        <!-- end .promo-text -->
		
<!--<nav class="main-nav">
			<div class="container">
				<div class="row">
					<div class="col-xs-3 wow fadeInUp animated" data-wow-delay="0.2s" data-wow-offset="50" data-wow-duration="1s">
						<p>Information</p>
						<ul class="main-nav__list">
							<li><a href="#">About Us</a></li>
							<li><a href="#">Delivery Information</a></li>
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">Terms &amp; Conditions</a></li>
						</ul>
					</div>
					<div class="col-xs-3 wow fadeInUp animated" data-wow-delay="0.4s" data-wow-offset="50" data-wow-duration="1s">
						<p>Extras</p>
						<ul class="main-nav__list">
							<li><a href="#">Brands</a></li>
							<li><a href="#">Gift Vouchers</a></li>
							<li><a href="#">Affiliates</a></li>
							<li><a href="#">Specials</a></li>
						</ul>
					</div>
					<div class="col-xs-3 wow fadeInUp animated" data-wow-delay="0.6s" data-wow-offset="50" data-wow-duration="1s">
						<p>My account</p>
						<ul class="main-nav__list">
							<li><a href="#">My Account</a></li>
							<li><a href="#">Order History</a></li>
							<li><a href="#">Wish List</a></li>
							<li><a href="#">Newsletter</a></li>
						</ul>
					</div>
					<div class="col-xs-3 wow fadeInUp animated" data-wow-delay="0.8s" data-wow-offset="50" data-wow-duration="1s">
						<p>Customer service</p>
						<ul class="main-nav__list">
							<li><a href="#">Contact Us</a></li>
							<li><a href="#">Returns</a></li>
							<li><a href="#">Site Map</a></li>
						</ul>
					</div>
				</div>
			</div>
		</nav>--><!-- end .main-nav -->
		<!--<div class="pay-systems">
			<div class="container">
				<div class="row">
					<div class="pay-systems__buffer">
						<ul>
							<li><a class="visa" href="#"></a></li>
							<li><a class="matercard" href="#"></a></li>
							<li><a class="webmoney" href="#"></a></li>
							<li><a class="alfa" href="#"></a></li>
							<li><a class="paypal" href="#"></a></li>
							<li><a class="unistream" href="#"></a></li>
							<li><a class="ukash" href="#"></a></li>
							<li><a class="easypay" href="#"></a></li>
							<li><a class="skrill" href="#"></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>--><!-- end .pay-systems -->
	</div>
    </div><!-- end .main -->
<?php //get_sidebar(); ?>
<?php get_footer(); ?>
